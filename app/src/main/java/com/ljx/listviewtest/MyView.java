package com.ljx.listviewtest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.util.jar.Attributes;

public class MyView extends View {
    Paint paint=new Paint();

    public MyView(Context context, Attributes attributes){
        super(context, (AttributeSet) attributes);

    }
    public MyView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }
    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        // TODO Auto-generated constructor stub
        canvas.drawCircle(300,300,200,paint);
    }
}
