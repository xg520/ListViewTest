package com.ljx.listviewtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

   private val fruits=ArrayList<Fruits>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        supportActionBar?.hide()
        initFruits() //初始化列表数据
//        val adapter=FruitAdapter(this,R.layout.fruit_item,fruits)
//        listview.adapter=adapter
//        listview.setOnItemClickListener { _,_ ,position,_ ->
//            val fruit=fruits[position]
//            Toast.makeText(this,fruit.name,Toast.LENGTH_SHORT).show()
//        }
        val layoutManager=StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL)
        recyclerView.layoutManager=layoutManager
//        layoutManager.orientation=LinearLayoutManager.HORIZONTAL
        val adapter=FruitAdapter2(fruits)
        recyclerView.adapter=adapter
    }
    private fun initFruits(){
        repeat(2){
           for (i in 0..20){
               fruits.add(Fruits(getRandomLengthString("测试文字"),R.mipmap.ic_launcher))
           }
        }
    }
    private fun getRandomLengthString(str:String):String{
        val n=(1..20).random()
        val builder=StringBuilder()
        repeat(n){
            builder.append(str)
        }
        return builder.toString()
    }
}
